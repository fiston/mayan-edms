# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Atdhe Tabaku <Atdhe617@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:02-0400\n"
"PO-Revision-Date: 2018-08-08 06:57+0000\n"
"Last-Translator: Atdhe Tabaku <Atdhe617@gmail.com>\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/rosarior/mayan-edms/language/bs_BA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs_BA\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:12 settings.py:9
msgid "Appearance"
msgstr "Izgled"

#: templates/403.html:5 templates/403.html:9
msgid "Insufficient permissions"
msgstr "Nedovoljno dozvola"

#: templates/403.html:11
msgid "You don't have enough permissions for this operation."
msgstr "Nemate odgovarajuca prava za ovu operaciju."

#: templates/404.html:5 templates/404.html:9
msgid "Page not found"
msgstr "Stranica nije pronađena"

#: templates/404.html:11
msgid "Sorry, but the requested page could not be found."
msgstr "Žao nam je, ali tražena stranica ne može biti pronađena."

#: templates/500.html:5 templates/500.html:9 templates/appearance/root.html:98
msgid "Server error"
msgstr "Greška u serveru"

#: templates/500.html:11
msgid ""
"There's been an error. It's been reported to the site administrators via "
"e-mail and should be fixed shortly. Thanks for your patience."
msgstr "Došlo je do greške. Prijavljeno je administratoru sajta putem e-pošte i trebalo bi da se popravi uskoro. Hvala na strpljenju."

#: templates/appearance/about.html:8 templates/appearance/about.html:57
msgid "About"
msgstr "O "

#: templates/appearance/about.html:62
msgid "Version"
msgstr "Verzija"

#: templates/appearance/about.html:64
#, python-format
msgid "Build number: %(build_number)s"
msgstr "Broj Izgradnje: %(build_number)s"

#: templates/appearance/about.html:77
msgid "Released under the license:"
msgstr "Obavljeno pod licensom:"

#: templates/appearance/base.html:33
msgid "Warning"
msgstr "Upozorenje"

#: templates/appearance/base.html:61
#: templates/navigation/generic_navigation.html:6
msgid "Actions"
msgstr "Akcije"

#: templates/appearance/base.html:63
msgid "Toggle Dropdown"
msgstr "Toggle Dropdown"

#: templates/appearance/calculate_form_title.html:16
#, python-format
msgid "Details for: %(object)s"
msgstr "Detalji o: %(object)s"

#: templates/appearance/calculate_form_title.html:19
#, python-format
msgid "Edit: %(object)s"
msgstr "Izmjeni: %(object)s"

#: templates/appearance/calculate_form_title.html:21
msgid "Create"
msgstr "Kreirati"

#: templates/appearance/dashboard_widget.html:25
msgid "View details"
msgstr "Pogledaj detalje"

#: templates/appearance/generic_confirm.html:6
#: templates/appearance/generic_confirm.html:13
msgid "Confirm"
msgstr "Potvrditi"

#: templates/appearance/generic_confirm.html:11
msgid "Confirm delete"
msgstr "Potvrditi brisanje"

#: templates/appearance/generic_confirm.html:27
#, python-format
msgid "Delete: %(object)s?"
msgstr "Obriši: %(object)s?"

#: templates/appearance/generic_confirm.html:48
msgid "Yes"
msgstr "Da"

#: templates/appearance/generic_confirm.html:52
msgid "No"
msgstr "Ne"

#: templates/appearance/generic_form_instance.html:49
#: templates/appearance/generic_form_instance.html:55
#: templates/appearance/generic_form_subtemplate.html:51
#: templates/appearance/generic_multiform_subtemplate.html:41
msgid "required"
msgstr "potrebno"

#: templates/appearance/generic_form_subtemplate.html:61
#: templates/appearance/generic_list_horizontal.html:21
#: templates/appearance/generic_list_items_subtemplate.html:118
#: templates/appearance/generic_list_subtemplate.html:109
msgid "No results"
msgstr "Nema rezultata"

#: templates/appearance/generic_form_subtemplate.html:76
#: templates/appearance/generic_multiform_subtemplate.html:63
msgid "Save"
msgstr "Sačuvati"

#: templates/appearance/generic_form_subtemplate.html:76
#: templates/appearance/generic_multiform_subtemplate.html:63
#: templates/authentication/password_reset_confirm.html:29
#: templates/authentication/password_reset_form.html:29
msgid "Submit"
msgstr "Podnijeti"

#: templates/appearance/generic_form_subtemplate.html:79
#: templates/appearance/generic_multiform_subtemplate.html:67
msgid "Cancel"
msgstr "Otkazati"

#: templates/appearance/generic_list_items_subtemplate.html:24
#: templates/appearance/generic_list_subtemplate.html:12
#, python-format
msgid ""
"Total (%(start)s - %(end)s out of %(total)s) (Page %(page_number)s of "
"%(total_pages)s)"
msgstr "Total (%(start)s - %(end)s od %(total)s) (Strana%(page_number)s od %(total_pages)s)"

#: templates/appearance/generic_list_items_subtemplate.html:26
#: templates/appearance/generic_list_items_subtemplate.html:29
#: templates/appearance/generic_list_subtemplate.html:14
#: templates/appearance/generic_list_subtemplate.html:17
#, python-format
msgid "Total: %(total)s"
msgstr "Total: %(total)s"

#: templates/appearance/generic_list_subtemplate.html:50
msgid "Identifier"
msgstr "Identifikator"

#: templates/appearance/home.html:9 templates/appearance/home.html:21
msgid "Dashboard"
msgstr "Komandna tabla"

#: templates/appearance/home.html:30
msgid "Getting started"
msgstr "Početak"

#: templates/appearance/home.html:33
msgid "Before you can fully use Mayan EDMS you need the following:"
msgstr "Prije nego što možete korisiti Mayan EDMS treba vam sljedeće:"

#: templates/appearance/home.html:54
msgid "Search pages"
msgstr "Potraž po stranici:"

#: templates/appearance/home.html:56 templates/appearance/home.html:66
msgid "Search"
msgstr "Pretraga"

#: templates/appearance/home.html:57 templates/appearance/home.html:67
msgid "Advanced"
msgstr "Napredni"

#: templates/appearance/home.html:64
msgid "Search documents"
msgstr "Pretraži dokumente"

#: templates/appearance/root.html:54
msgid "Toggle navigation"
msgstr "Prebacite navigaciju"

#: templates/appearance/root.html:103
msgid "Close"
msgstr "Zatvori"

#: templates/appearance/root.html:122
msgid "Server communication error"
msgstr "Problem u komunikaciji sa serverom"

#: templates/appearance/root.html:124
msgid "Check you network connection and try again in a few moments."
msgstr "Proverite mrežnu vezu i pokušajte ponovo za nekoliko trenutaka."

#: templates/authentication/login.html:10
msgid "Login"
msgstr "Prijava"

#: templates/authentication/login.html:21
msgid "First time login"
msgstr "Prijava - prvi put"

#: templates/authentication/login.html:25
#, python-format
msgid ""
"You have just finished installing <strong>%(project_title)s</strong>, "
"congratulations!"
msgstr "Upravo ste završili instalaciju <strong>%(project_title)s</strong>, čestitam!"

#: templates/authentication/login.html:26
msgid "Login using the following credentials:"
msgstr "Prijava korištenjem sljedećih podataka:"

#: templates/authentication/login.html:27
#, python-format
msgid "Username: <strong>%(account)s</strong>"
msgstr "Korisnik: <strong>%(account)s</strong>"

#: templates/authentication/login.html:28
#, python-format
msgid "Email: <strong>%(email)s</strong>"
msgstr "Email: <strong>%(email)s</strong>"

#: templates/authentication/login.html:29
#, python-format
msgid "Password: <strong>%(password)s</strong>"
msgstr "Pasvord: <strong>%(password)s</strong>"

#: templates/authentication/login.html:30
msgid ""
"Be sure to change the password to increase security and to disable this "
"message."
msgstr "Ne zaboravite promijeniti pasvord da pojačate sigurnost i onemogućite dalje prikazivanje ove poruke."

#: templates/authentication/login.html:46
#: templates/authentication/login.html:54
msgid "Sign in"
msgstr "Prijavite se"

#: templates/authentication/login.html:59
msgid "Forgot your password?"
msgstr "Zaboravili lozinku?"

#: templates/authentication/password_reset_complete.html:8
#: templates/authentication/password_reset_confirm.html:8
#: templates/authentication/password_reset_confirm.html:20
#: templates/authentication/password_reset_done.html:8
#: templates/authentication/password_reset_form.html:8
#: templates/authentication/password_reset_form.html:20
msgid "Password reset"
msgstr "Resetovanje lozinke"

#: templates/authentication/password_reset_complete.html:15
msgid "Password reset complete! Click the link below to login."
msgstr "Resetovanje lozinke završeno! Kliknite na link ispod kako biste se prijavili."

#: templates/authentication/password_reset_complete.html:17
msgid "Login page"
msgstr "Prijava na stranicu"

#: templates/authentication/password_reset_done.html:15
msgid "Password reset email sent!"
msgstr "Resetovana lozinka poslata na vaš email!"

#: templatetags/appearance_tags.py:16
msgid "None"
msgstr "Nijedno"
