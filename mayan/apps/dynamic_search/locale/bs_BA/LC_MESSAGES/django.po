# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Ilvana Dollaroviq <ilvanadollaroviq@gmail.com>, 2018
# www.ping.ba <jomer@ping.ba>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:04-0400\n"
"PO-Revision-Date: 2018-08-09 10:21+0000\n"
"Last-Translator: Ilvana Dollaroviq <ilvanadollaroviq@gmail.com>\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/rosarior/mayan-edms/language/bs_BA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs_BA\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:16
msgid "Dynamic search"
msgstr "Dinamičko pretraživanje"

#: classes.py:33
msgid "No search model matching the query"
msgstr "Nema tražnog modela koji odgovara upitu"

#: forms.py:9
msgid "Match all"
msgstr "Složi sve"

#: forms.py:10
msgid ""
"When checked, only results that match all fields will be returned. When "
"unchecked results that match at least one field will be returned."
msgstr "Kada se proveri, biće vraćeni samo rezultati koji odgovaraju svim poljima. Kada se neprovereni rezultati koji se podudaraju sa najmanje jednim poljem će biti vraćeni."

#: forms.py:29
msgid "Search terms"
msgstr "Pojmovi pretrage"

#: links.py:8 settings.py:8 views.py:52 views.py:63
msgid "Search"
msgstr "Pretraga"

#: links.py:11 views.py:77
msgid "Advanced search"
msgstr "Napredna pretraga"

#: links.py:15
msgid "Search again"
msgstr "Traži ponovo"

#: settings.py:11
msgid "Maximum amount search hits to fetch and display."
msgstr "Maksimalni broj pretražnih hitova da se rezultati prikažu."

#: views.py:25
#, python-format
msgid "Search results for: %s"
msgstr "Traži rezultate za:%s"

#: views.py:65
#, python-format
msgid "Search for: %s"
msgstr "Traži za:%s"
